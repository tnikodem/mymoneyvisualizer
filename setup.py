#!/usr/bin/env python
# -*- coding: utf-8 -*-
import io
import re
from collections import OrderedDict

from setuptools import setup

with io.open('README.md', 'rt', encoding='utf8') as f:
    readme = f.read()

with io.open('mymoneyvisualizer/__init__.py', 'rt', encoding='utf8') as f:
    version = re.search(r'__version__ = \'(.*?)\'', f.read()).group(1)

setup(
    name='MyMoneyVisualizer',
    version=version,
    license='MIT',
    author='Thomas Nikodem',
    description='A simple tool to visualize your income and expenses',
    long_description=readme,
    packages=['mymoneyvisualizer'],
    include_package_data=True,
    zip_safe=False,
    platforms='any',
    python_requires='>=3.6',
    install_requires=[
        'numpy==1.15.*',
        'pandas==0.23.*',
        'pyqt5==5.11.*',
        'pyyaml==3.13',
    ],
    extras_require={
        'dev': [
            'pytest==4.0.*',
            'pytest-qt==3.2.*',
            'pytest-cov==2.6.*',
        ]
    },
    entry_points={
        'console_scripts': [
            'mmv = mymoneyvisualizer.cli:main',
        ],
    },
)


