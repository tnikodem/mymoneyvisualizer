# -*- coding: utf-8 -*-
import logging
import datetime
import pandas as pd

from PyQt5.QtWidgets import QWidget, QMainWindow, QVBoxLayout
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtWidgets import QTableWidget, QTableWidgetItem
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QColor


from mymoneyvisualizer.naming import Naming as nn

logger = logging.getLogger(__name__)


class MyTableWidget(QWidget):
    def __init__(self, parent, main):
        super(QWidget, self).__init__(parent)
        self.main = main

        self.columns = []

        self.tableWidget = QTableWidget()
        self.tableWidget.verticalHeader().setVisible(False)
        self.tableWidget.setSortingEnabled(True)
        self.tableWidget.setEditTriggers(QTableWidget.NoEditTriggers)

        # always exactly 12 month (tag, months ... , total, mthly average)
        self.tableWidget.setColumnCount(15)
        self.tableWidget.setColumnWidth(0, 180)
        for i in range(1, 14):
            self.tableWidget.setColumnWidth(i, 80)

        self.layout = QVBoxLayout(self)
        self.layout.addWidget(self.tableWidget)
        self.setLayout(self.layout)

        # add actions
        self.tableWidget.doubleClicked.connect(self.open_window_detail)
        self.main.config.accounts.add_update_callback(self.update_table)

        self.update_table()

    def open_window_summary_tag(self, tag):
        # TODO implement tag overview window (similar to monthly overview, however without month cut
        logger.error("not implemented")

    def open_window_detail(self, item):
        if item.column() == 0:
            self.open_window_summary_tag(item.data())
        else:
            month = self.columns[item.column()]
            tag = self.tableWidget.item(item.row(), 0).text()
            self.main.open_detailmonth(month=month, tag=tag)

    def update_table(self):
        df = self.main.summary_df()
        if df is None:
            self.tableWidget.setRowCount(0)
            return
        self.columns = df.columns
        self.tableWidget.setHorizontalHeaderLabels(self.columns)
        self.tableWidget.setRowCount(len(df))
        for i, row in df.iterrows():
            for j, col in enumerate(df.columns):
                if isinstance(row[col], (int, float)):
                    item = QTableWidgetItem()
                    item.setData(Qt.EditRole, float(round(row[col], 0)))
                else:
                    item = QTableWidgetItem(str(row[col]))
                if col == "total" or col == "monthly average" or i == len(df) - 1:
                    item.setBackground(QColor(0, 0, 0, 100))
                self.tableWidget.setItem(i, j, item)


class SummaryWidget(QWidget):
    def __init__(self, parent, main):
        super(QWidget, self).__init__(parent)
        self.main = main

        self.layout = QVBoxLayout(self)
        self.setLayout(self.layout)

        self.button_before = QPushButton('previous month', self)
        self.button_before.resize(250, 32)
        self.button_before.move(40, 0)
        self.button_before.clicked.connect(self.main.month_before)

        self.button_after = QPushButton('next month', self)
        self.button_after.resize(250, 32)
        self.button_after.move(340, 0)
        self.button_after.clicked.connect(self.main.month_after)

        self.table = MyTableWidget(parent=self, main=self.main)
        self.table.resize(1700, 600)
        self.table.move(40, 80)


class WindowSummary(QMainWindow):
    def __init__(self, parent, config, detail_month_window):
        super(WindowSummary, self).__init__(parent)
        self.config = config
        self.detail_month_window = detail_month_window

        self.title = 'Summary'
        self.left = 50
        self.top = 50
        self.width = 1700
        self.height = 800
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        self.date_from, self.date_upto = None, None
        self.set_current_date_window()

        self.summary_widget = SummaryWidget(parent=self, main=self)
        self.summary_widget.resize(1600, 700)
        self.summary_widget.move(30, 30)

    def set_current_date_window(self):
        now = datetime.datetime.now()
        date_upto = datetime.datetime(now.year, now.month, 1) + datetime.timedelta(days=32)  # mth at most 31 days
        self.date_upto = datetime.datetime(date_upto.year, date_upto.month, 1)
        self.date_from = datetime.datetime(date_upto.year - 1, date_upto.month, 1)

    def month_before(self):
        date_upto = self.date_upto - datetime.timedelta(days=2)
        self.date_upto = datetime.datetime(date_upto.year, date_upto.month, 1)
        self.date_from = datetime.datetime(date_upto.year - 1, date_upto.month, 1)
        self.summary_widget.table.update_table()

    def month_after(self):
        date_upto = self.date_upto + datetime.timedelta(days=32)  # month at most 31 days
        self.date_upto = datetime.datetime(date_upto.year, date_upto.month, 1)
        self.date_from = datetime.datetime(date_upto.year - 1, date_upto.month, 1)
        self.summary_widget.table.update_table()

    def open_detailmonth(self, month, tag):
        self.detail_month_window.open_detailmonth(month=month, tag=tag)

    @staticmethod
    def get_base_tag(tag):
        tag = str(tag)
        tags = tag.split(".")
        tag_out = tags[0]
        return tag_out

    # TODO unittest method
    def summary_df(self):
        logger.debug(f"request summary df from {self.date_from} upto {self.date_upto}")
        dfs = []
        account_names = []
        for acc in self.config.accounts.get():
            account_names += [acc.name]
            df = acc.df
            df = df[(df[nn.date] >= self.date_from) & (df[nn.date] < self.date_upto)]
            if len(df) < 1:
                continue
            dfs += [df]

        if len(dfs) < 1:
            return
        df = pd.concat(dfs, sort=False)
        df = df.loc[~(df[nn.tag].isin(account_names)), :]
        if len(df) < 1:
            return

        temp_dates = pd.date_range(self.date_from, self.date_upto, freq='1M')
        temp_values = pd.DataFrame({nn.date: temp_dates,
                                    nn.value: [0]*len(temp_dates),
                                    nn.tag: ["temp_tag_43643564356345"]*len(temp_dates),
                                    })
        df = pd.concat([df, temp_values], sort=False)

        df[nn.tag] = df[nn.tag].apply(self.get_base_tag)

        # calculate pivot table
        dfg = df.groupby([nn.tag, pd.Grouper(freq="1M", key=nn.date)]).agg({nn.value: "sum"}).reset_index()
        dfp = dfg.pivot(index=nn.tag, columns=nn.date, values=["value"]).fillna(0)
        dfp = dfp[dfp.index != "temp_tag_43643564356345"]
        dfp.columns = [t.strftime('%Y-%m') for a, t in dfp.columns.ravel()]
        ncol = len(dfp.columns)
        dfp["total"] = dfp.sum(axis=1)
        dfp["monthly average"] = dfp["total"] / ncol
        dfp = dfp.sort_values(["total"], ascending=True)
        dfp = dfp.append(dfp.sum().rename("total"))
        dfp = dfp.reset_index()
        logger.debug(f"return summary_df with len {len(dfp)}")
        return dfp
